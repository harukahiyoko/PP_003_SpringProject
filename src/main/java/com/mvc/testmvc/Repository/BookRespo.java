package com.mvc.testmvc.Repository;


import com.github.javafaker.Faker;
import com.mvc.testmvc.Model.Book;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;

@Repository
public class BookRespo {
    Faker faker = new Faker();
    List<Book> data = new ArrayList();

    {
        for (int i = 1; i <= 10; i++) {
            Book book = new Book();
            book.setId(i);
            book.setName(faker.book().title());
            book.setAuthor(faker.book().author());
            book.setPublisher(faker.book().publisher());

            data.add(book);
        }
    }

    public List<Book> getAll(){
        return data;
    }
    public void create( Book book){
        data.add(book);
    }
    public Book findOne( Integer id){
        for (int i = 0; i < data.size(); i++)
        {
            if (data.get(i).getId() == id){
                return data.get(i);
            }
        }
        return null;
    }

    public  boolean update(Book book){
        for (int i = 0; i < data.size(); i++) {
            if (data.get(i).getId() == book.getId()) {
                data.set(i, book);
                return true;
            }
        }
        return false;
    }
    public  boolean delete(Integer id){
        for (int i = 0; i < data.size(); i++) {
            if (data.get(i).getId() == id){
                data.remove(i);
                return true;
            }
        }
        return false;
    }
}
