package com.mvc.testmvc.Controller;

import com.mvc.testmvc.Model.Book;
import com.mvc.testmvc.Service.BookService;
import com.mvc.testmvc.Service.UploadService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.Valid;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;
import java.util.UUID;

@Controller
public class BookController {

    private BookService bookService;

    private UploadService uploadService;

    public BookController(BookService bookService, UploadService uploadService) {
        this.bookService = bookService;
        this.uploadService = uploadService;
    }

    @GetMapping("/")
    public String index(ModelMap modelMap){
        List<Book> book = bookService.getData();
        modelMap.addAttribute("books",book);
        return "index";
    }

    @GetMapping("/view/{id}")
    public String viewDetail(@PathVariable("id") Integer id, Model m){

        m.addAttribute("book", this.bookService.findOne(id));

        return "view";
    }

    @GetMapping("/update/{id}")
    public  String update(@PathVariable Integer id, Model m){

        m.addAttribute("book", this.bookService.findOne(id));

        return "update";
    }

    @PostMapping("/update/submit")
    public  String updateSubmit(@ModelAttribute Book book, MultipartFile file){

        String filename = this.uploadService.singleFileUpload(file, "img");

        if(!file.isEmpty()){
            book.setThumbnail("/images-pp/"+filename);
        }

        this.bookService.update(book);
        return "redirect:/";
    }

    @GetMapping("/remove/{id}")
    public String delete(@PathVariable Integer id){
        this.bookService.delete(id);
        return "redirect:/";

    }

    @GetMapping("/create")
    public String create(Model model){
        model.addAttribute("book",new Book());
        return "create";
    }

    @PostMapping("/create/submit")
    public String createSubmit(@Valid Book book, BindingResult bindingResult, MultipartFile file){

        System.out.println(book);

        if(bindingResult.hasErrors()){
            return "create";
        }

        String filename = this.uploadService.singleFileUpload(file, "img");

        book.setThumbnail("/images-pp/"+filename);

        this.bookService.create(book);

        return "redirect:/";
    }
}
