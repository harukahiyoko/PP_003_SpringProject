package com.mvc.testmvc.Model;


import javax.validation.constraints.Positive;
import javax.validation.constraints.Size;

public class Book {

    @Positive
    private int id;

    @Size(min = 5, max = 10)
    private String name;

    private String author;
    private String publisher;
    private String thumbnail;
    public Book() {
    }

    public Book(@Positive int id, @Size(min = 5, max = 10) String name, String author, String publisher, String thumbnail) {
        this.id = id;
        this.name = name;
        this.author = author;
        this.publisher = publisher;
        this.thumbnail = thumbnail;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getPublisher() {
        return publisher;
    }

    public void setPublisher(String publisher) {
        this.publisher = publisher;
    }

    public String getThumbnail() {
        return thumbnail;
    }

    public void setThumbnail(String thumbnail) {
        this.thumbnail = thumbnail;
    }

    @Override
    public String toString() {
        return "Book{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", author='" + author + '\'' +
                ", publisher='" + publisher + '\'' +
                ", thumbnail='" + thumbnail + '\'' +
                '}';
    }
}
