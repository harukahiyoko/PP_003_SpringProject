package com.mvc.testmvc.Service;

import org.springframework.web.multipart.MultipartFile;

public interface UploadServiceInterface {
    String singleFileUpload(MultipartFile file, String folder);
}
