package com.mvc.testmvc.Service;

import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.UUID;

@Service
public class UploadService implements UploadServiceInterface {
    @Override
    public String singleFileUpload(MultipartFile file, String folder) {

        if(file.isEmpty())
            return null;

        File path = new File("/pp6th/"+ folder);

        if (!path.exists())
            path.mkdirs();

        String filename= file.getOriginalFilename();

        String extension = filename.substring(filename.lastIndexOf('.') + 1);

        filename = UUID.randomUUID() + "." +extension;

        try {
            Files.copy(file.getInputStream(), Paths.get("/pp6th/"+folder,filename));
        } catch (IOException e){

        }

        return folder +"/"+ filename;
    }
}
