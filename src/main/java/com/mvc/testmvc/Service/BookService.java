package com.mvc.testmvc.Service;

import com.mvc.testmvc.Model.Book;
import com.mvc.testmvc.Repository.BookRespo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class BookService implements BookInterface {

    private BookRespo bookRespo;

    @Autowired
    public BookService(BookRespo bookRespo) {
        this.bookRespo = bookRespo;
    }

    @Override
    public List<Book> getData() {
        return this.bookRespo.getAll();
    }

    @Override
    public Book findOne(Integer id) {
        return this.bookRespo.findOne(id);
    }

    @Override
    public boolean update(Book book) {
        return this.bookRespo.update(book);
    }

    @Override
    public boolean delete(Integer id) {
        return this.bookRespo.delete(id);
    }

    public void create(Book book){
        this.bookRespo.create(book);
    }
}
