package com.mvc.testmvc.Service;

import com.mvc.testmvc.Model.Book;

import java.util.List;

public interface BookInterface {
    List<Book> getData();
    Book findOne(Integer id);
    boolean update(Book book);
    boolean delete(Integer id);


}
